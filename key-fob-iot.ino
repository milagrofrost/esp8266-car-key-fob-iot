/*
 * This is an example on how to use Espalexa alongside an ESPAsyncWebServer.
 */

#define ESPALEXA_ASYNC //it is important to define this before #include <Espalexa.h>!
#include <Espalexa.h>


#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>

#include <ESPAsyncWebServer.h>

#include <ArduinoOTA.h>

// START CUSTOMIZATIONS //

// Set your Static IP address
IPAddress ip(10, 10, 101, 69);
// Set your Gateway IP address
IPAddress gateway(10, 10, 101, 1);

IPAddress subnet(255, 255, 255, 0);
IPAddress dns(10, 10, 101, 1);

const char* hostname = "CarServer";

// Change this!!
const char* ssid = "xxxxxxx";
const char* password = "xxxxxxxx";

const char* remoteUploadPassword = "123";

const uint8_t lockPin = 4;
const uint8_t unlockPin = 5;
const uint8_t startPin = 16;

// END CUSTOMIZATIONS //

char* initiateCommand = "initial";
unsigned long time_now = 0;
int countUp = 0;
int stepInt = 1;

boolean wifiConnected = false;

Espalexa espalexa;
AsyncWebServer server(80);


void setup()
{

  pinMode(lockPin, OUTPUT);
  pinMode(unlockPin, OUTPUT);
  pinMode(startPin, OUTPUT);

  digitalWrite(lockPin, 1);
  digitalWrite(unlockPin, 1);
  digitalWrite(startPin, 1);

  Serial.begin(115200);
  // Initialise wifi connection

  wifiConnected = connectWifi();

  // Testing crashes
  ardunioOTAstart();

  if(wifiConnected){
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(200, "text/plain", "This page has loaded successfully");

      Serial.println("Root path has been hit\n");

    });

    server.on("/lock", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(200, "text/plain", "A command was sent to lock the car");
      Serial.print("Car has been: LOCKED with HTTP\n");
      initiateCommand = "carLock";
      time_now = millis();
      stepInt = 1;
    });

    server.on("/unlock", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(200, "text/plain", "A command was sent to unlock the car");
      Serial.print("Car has been: UNLOCKED with HTTP\n");
      initiateCommand = "carUnlock";
      time_now = millis();
      stepInt = 1;
    });

    server.on("/start", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(200, "text/plain", "A command was sent to start the car");
      Serial.print("Car has been: STARTED with HTTP\n");
      initiateCommand = "carStart";
      time_now = millis();
      stepInt = 1;
    });

    server.on("/test", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(200, "text/plain", "test page");
      Serial.print("I've hit test page\n");
      initiateCommand = "carTest";
      time_now = millis();
      stepInt = 1;
    });


    server.onNotFound([](AsyncWebServerRequest *request){
      if (!espalexa.handleAlexaApiCall(request)) //if you don't know the URI, ask espalexa whether it is an Alexa control request
      {
        //whatever you want to do with 404s
        request->send(404, "text/plain", "Not found");
      }
    });

    // Define your devices here.
    espalexa.addDevice("car locks", carLockCommand );
    espalexa.addDevice("car engine", carEngineCommand );

    espalexa.begin(&server); //give espalexa a pointer to your server object so it can use your server instead of creating its own
    //server.begin(); //omit this since it will be done by espalexa.begin(&server)
  } else
  {
    while (1)
    {
      Serial.println("Cannot connect to WiFi. Please check data and reset the ESP.");
      delay(2500);
    }
  }
}

void loop()
{
   espalexa.loop();
   delay(100);
   if (initiateCommand == "carTest") {
     countUp = (unsigned long)(millis() - time_now);
     carTest();
   } else if (initiateCommand == "carLock") {
     countUp = (unsigned long)(millis() - time_now);
     carLock();
   } else if (initiateCommand == "carUnlock") {
     countUp = (unsigned long)(millis() - time_now);
     carUnlock();
   } else if (initiateCommand == "carStart") {
     countUp = (unsigned long)(millis() - time_now);
     carStart();
   } else {

   }
}

//our callback functions
void carLockCommand(const uint8_t brightness) {
    Serial.println("Car has been: ");

    //do what you need to do here

    //EXAMPLE
    if (brightness == 255) {
      Serial.println("LOCKED with Alexa");
      initiateCommand = "carLock";
      time_now = millis();
      stepInt = 1;
    }
    else if (brightness == 0) {
      Serial.println("UNLOCKED with Alexa");
      initiateCommand = "carUnlock";
      time_now = millis();
      stepInt = 1;
    }
    else {
      Serial.println("Nothing happens");
    }
}

void carEngineCommand(const uint8_t brightness) {
    Serial.print("Car has been: ");

    //do what you need to do here

    //EXAMPLE
    if (brightness == 255) {
      Serial.println("STARTED with Alexa");
      initiateCommand = "carStart";
      time_now = millis();
      stepInt = 1;
    }
    else if (brightness == 0) {
      Serial.println("Nothing happens");
    }
    else {
      Serial.println("Nothing happens");
    }
}


void ardunioOTAstart(){
 // Hostname defaults to esp8266-[ChipID]
 ArduinoOTA.setHostname(hostname);

 // No authentication by default
 ArduinoOTA.setPassword(remoteUploadPassword);

 ArduinoOTA.onStart([]() {
   Serial.println("Start");
 });

 ArduinoOTA.onEnd([]() {
   Serial.println("\nEnd");
 });

 ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
   Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
 });

 ArduinoOTA.onError([](ota_error_t error) {
   Serial.printf("Error[%u]: ", error);
   if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
   else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
   else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
   else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
   else if (error == OTA_END_ERROR) Serial.println("End Failed");
 });

 ArduinoOTA.begin();
}

// connect to wifi – returns true if successful or false if not
boolean connectWifi(){
  boolean state = true;
  int i = 0;
  WiFi.mode(WIFI_STA);
  WiFi.config(ip, gateway, subnet, dns);
  WiFi.hostname(hostname);
  WiFi.begin(ssid, password);
  Serial.println("");
  Serial.println("Connecting to WiFi");

  // Wait for connection
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    if (i > 20){
      state = false; break;
    }
    i++;
  }
  Serial.println("");
  if (state){
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
  else {
    Serial.println("Connection failed.");
  }
  delay(100);
  return state;
}

void carTest() {

  if(countUp > 500 && stepInt == 1){
    Serial.print("1st step\n");
    ++stepInt;
  };

  if(countUp > 1000 && stepInt == 2){
    Serial.print("2nd step\n");
    ++stepInt;
  };

  if(countUp > 1500 && stepInt == 3){
    Serial.print("3rd step\n");

    initiateCommand = "done";
  };

  if(countUp > 2000 && stepInt == 4){
    Serial.print("4th step\n");

    initiateCommand = "done";
  };

}

void carLock() {

  if(countUp > 0 && stepInt == 1){
    Serial.print("resetPins()\n");
    resetPins();
    ++stepInt;
  };

  if(countUp > 500 && stepInt == 2){
    Serial.print("lockPin, 0\n");
    digitalWrite(lockPin, 0);
    ++stepInt;
  };

  if(countUp > 1000 && stepInt == 3){
    Serial.print("lockPin, 1\n");
    digitalWrite(lockPin, 1);
    ++stepInt;
  };

  if(countUp > 1500 && stepInt == 4){
    Serial.print("lockPin, 0\n");
    digitalWrite(lockPin, 0);
    ++stepInt;
  };

  if(countUp > 2000 && stepInt == 5){
    Serial.print("lockPin, 1\n");
    digitalWrite(lockPin, 1);
    initiateCommand = "done";
  };

}

void carUnlock() {

  if(countUp > 0 && stepInt == 1){
    Serial.print("resetPins()\n");
    resetPins();
    ++stepInt;
  };

  if(countUp > 500 && stepInt == 2){
    Serial.print("unlockPin, 0\n");
    digitalWrite(unlockPin, 0);
    ++stepInt;
  };

  if(countUp > 1000 && stepInt == 3){
    Serial.print("unlockPin, 1\n");
    digitalWrite(unlockPin, 1);
    ++stepInt;
  };

  if(countUp > 1500 && stepInt == 4){
    Serial.print("unlockPin, 0\n");
    digitalWrite(unlockPin, 0);
    ++stepInt;
  };

  if(countUp > 2000 && stepInt == 5){
    Serial.print("unlockPin, 1\n");
    digitalWrite(unlockPin, 1);
    initiateCommand = "done";
  };

}

void carStart() {

  if(countUp > 0 && stepInt == 1){
    Serial.print("resetPins()\n");
    resetPins();
    ++stepInt;
  };

  if(countUp > 500 && stepInt == 2){
    Serial.print("lockPin, 0\n");
    digitalWrite(lockPin, 0);
    ++stepInt;
  };

  if(countUp > 1000 && stepInt == 3){
    Serial.print("lockPin, 1\n");
    digitalWrite(lockPin, 1);
    ++stepInt;
  };

  if(countUp > 1500 && stepInt == 4){
    Serial.print("lockPin, 0\n");
    digitalWrite(lockPin, 0);
    ++stepInt;
  };

  if(countUp > 2000 && stepInt == 5){
    Serial.print("lockPin, 1\n");
    digitalWrite(lockPin, 1);
    ++stepInt;
  };

  if(countUp > 3000 && stepInt == 6){
    Serial.print("startPin, 0\n");
    digitalWrite(startPin, 0);
    ++stepInt;
  };
  if(countUp > 6500 && stepInt == 7){
    Serial.print("startPin, 1\n");
    digitalWrite(startPin, 1);
    ++stepInt;
  };

  if(countUp > 8000 && stepInt == 8){
    Serial.print("unlockPin, 0\n");
    digitalWrite(unlockPin, 0);
    ++stepInt;
  };

  if(countUp > 8500 && stepInt == 9){
    Serial.print("unlockPin, 1\n");
    digitalWrite(unlockPin, 1);
    ++stepInt;
  };

  if(countUp > 9000 && stepInt == 10){
    Serial.print("unlockPin, 0\n");
    digitalWrite(unlockPin, 0);
    ++stepInt;
  };

  if(countUp > 9500 && stepInt == 11){
    Serial.print("unlockPin, 1\n");
    digitalWrite(unlockPin, 1);
    initiateCommand = "done";
  };


}

void resetPins() {
    digitalWrite(startPin, 1);
    digitalWrite(unlockPin, 1);
    digitalWrite(lockPin, 1);
}
