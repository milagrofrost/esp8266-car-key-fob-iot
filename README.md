Inspirations and helpful links I've used to make this: 

https://www.hackster.io/user03583/ok-google-start-my-car-7088dd

https://randomnerdtutorials.com/alexa-echo-with-esp32-and-esp8266/

https://www.norwegiancreations.com/2018/10/arduino-tutorial-avoiding-the-overflow-issue-when-using-millis-and-micros/?fbclid=IwAR2p51uPdSbIAHToBQ1HVKb6hG61MZ5QO-jllXi2N6Qzv-MIi-0fyspchBM


I have a 2009 Chevrolet Suburban 2500 and wanted to IoT it. I've seen some people hack their wiring in their vehicle or get into their car's computer to make it do the work of locking/unlock/starting, but I wanted to go with an approach that would not mod anything to the car itself.

Ended up just buying a new key fob, programmed it to the car using the car's built-in fob learning method. Then took it apart and started playing. I poked around the fob circuitry, trying to understand how to properly get each "button" to ground properly using the ESP8266. Ended up needing to use diodes at the button and input voltage to make sure electricity was moving in the right direction.

I have this hooked up with Alexa as a "Light" device, so asking to do certain things is kinda odd. To unlock the car you would say "Alexa, turn [car locks] off". So in the Alexa app I created routines for all 3 commands so that I could more naturally ask her to "unlock the car" and it would do the same as the " turn [car locks] off".

There's also a web server built-in to kick off the IoT commands as well. I use the built-in webserver for integration with Home Assistant. I schedule a webhook to hit the "lock" URL every night at 11:30PM and also will "Lock" and horn chirp when motion is detected outside the house with the Ring camera after midnight until morning.

The downside to this method is that there is no feedback other than hearing horn chirps/ engine running or lights flashing to confirm that the key fob signal had been received. The ESP8266 is just sending the command with best effort. Been playing the idea of having another ESP8266 in the car and when the car turns on, it will power the USB for that ESP8266 and will confirm that the engine is running.

There was a lot trickery to make delays work between button presses in the code since Asynchronous code and delay() do not mix! Using millis() instead.

Also made a custom 3D printed box for the whole deal. That's included in the gitlab repo link below. I've included other helpful links that I used in this project in the repo.
